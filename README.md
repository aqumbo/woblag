# Woblag

Woblag is a Docker image for running Hugo together with Caddy as the server and 
using webhooks to automatically generate the files on updates.

## How to use

Edit the docker-compose.yml and start it with:

```
$ docker-compose up -d
```

This will launch Caddy with certificate from LetsEncrypt and autogenerate your blog from the repository.

Here's an example [repo](https://gitlab.com/aqumbo/woblag-example-repo) you can use.

## License

See the [LICENSE](LICENSE.md) file for license rights and limitations (MIT).
